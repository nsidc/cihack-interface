# webpack-react-template

Example application using Webpack and React.
Other notable features include React Hot Reloader and written with es2016.

## Development

Install dependencies with `npm install`

Run the app with `npm start`
