FROM nginx:1.9.2

COPY build/ /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf

COPY cihack-interface.conf /etc/nginx/conf.d/